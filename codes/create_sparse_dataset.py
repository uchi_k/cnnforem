# coding:utf-8
import Image, os, glob, sys
import numpy as np
import shutil

# 絶対パス取得(CNNforEM/からスクリプトを走らせる想定)
abs_path = os.getcwd()
os.chdir('%s/2014EMneuron/train-input/tif_dataset' % abs_path)
filelist = glob.glob('*.tif') #tif対応

#コマンドラインからsplitｔを受け取る
argvs = sys.argv #コマンドライン引数のリスト取得
argc = len(argvs) #引数の個数取得
if (argc != 2):
    print 'Usage: コマンドライン引数が' + '%s' % (argc - 1) + 'コで多いもしくは少ないです・・・'
    quit()

if os.path.exists('%s/2014EMneuron/train-input/tif_dataset/' % abs_path + 'iso_2d_%s_training' % int(argvs[1])):
    shutil.rmtree('iso_2d_%s_training' % argvs[1])

os.mkdir('iso_2d_%s_training' % argvs[1])# saveで自動で作られる可能性がある。その場合不要

f = open('iso_2d_%s_train.txt' % argvs[1], 'w')
split = int(argvs[1])
file_num = 1

print "creating iso_2d_%s_training dataset..." % argvs[1]
for file in filelist:
    im = Image.open('%s' % file)

    range = (0, 0, 1024, 1000)
    sp_list = np.array(list(im.crop(range).getdata())).reshape((1000, 1024))
    sp_im = Image.fromarray(np.uint8(sp_list))

    for i in xrange(0, 100):
        range = (0, i * 10, 1024, (i + 1) * 10)
        if i == 0:
            belt = sp_im.crop(range)
            belt = np.array(list(belt.getdata())).reshape(10, 1024)[0]
        else:
            belt = np.vstack((belt, np.array(list(sp_im.crop(range).getdata())).reshape(10, 1024)[0]))

    im_belt = Image.fromarray(np.uint8(belt))

    for w in xrange(0, 100):
        for h in xrange(0, 10):
            range = (w * 10, h * 10, split + 1 + w * 10, split + 1 + h * 10)
            square = im_belt.crop(range)
            ar_square = np.array(list(square.getdata())).reshape((33, 33))
            new_square = np.vstack((ar_square[0:16], ar_square[17:33]))
            ans = ar_square[16][16]
            im_square = Image.fromarray(np.uint8(new_square))
            im_square.save('iso_2d_%s_training/iso_2d' % argvs[1] + '_%03d' % file_num + '%03d' % w + '%03d' % h + '.tif')
            f.write('iso_2d_%03d' % file_num + '%03d' % w + '%03d' % h + '.tif' + ' %s\n' % ans)
    if file_num % 10 == 0:
        print "%s epoch ended" % file_num
    file_num += 1
print "iso_2d_%s_training dataset created" % argvs[1]


os.chdir('%s/2014EMneuron/test-input/tif_dataset' % abs_path)
filelist = glob.glob('*.tif') #tif対応

if os.path.exists('%s/2014EMneuron/test-input/tif_dataset/' % abs_path + 'iso_2d_%s_test' % int(argvs[1])):
    shutil.rmtree('iso_2d_%s_test' % argvs[1])

os.mkdir('iso_2d_%s_test' % argvs[1])# saveで自動で作られる可能性がある。その場合不要

f = open('iso_2d_%s_test.txt' % argvs[1], 'w')
file_num = 1

print "creating iso_2d_%s_test dataset..." % argvs[1]
for file in filelist:
    im = Image.open('%s' % file)

    range = (0, 0, 1024, 1000)
    sp_list = np.array(list(im.crop(range).getdata())).reshape((1000, 1024))
    sp_im = Image.fromarray(np.uint8(sp_list))

    for i in xrange(0, 100):
        range = (0, i * 10, 1024, (i + 1) * 10)
        if i == 0:
            belt = sp_im.crop(range)
            belt = np.array(list(belt.getdata())).reshape(10, 1024)[0]
        else:
            belt = np.vstack((belt, np.array(list(sp_im.crop(range).getdata())).reshape(10, 1024)[0]))

    im_belt = Image.fromarray(np.uint8(belt))

    for w in xrange(0, 100):
        for h in xrange(0, 10):
            range = (w * 10, h * 10, split + 1 + w * 10, split + 1 + h * 10)
            square = im_belt.crop(range)
            ar_square = np.array(list(square.getdata())).reshape((33, 33))
            new_square = np.vstack((ar_square[0:16], ar_square[17:33]))
            ans = ar_square[16][16]
            im_square = Image.fromarray(np.uint8(new_square))
            im_square.save('iso_2d_%s_test/iso_2d' % argvs[1] + '_%03d' % file_num + '%03d' % w + '%03d' % h + '.tif')
            f.write('iso_2d_%03d' % file_num + '%03d' % w + '%03d' % h + '.tif' + ' %s\n' % ans)
    if file_num % 10 == 0:
        print "%s epoch ended" % file_num
    file_num += 1
print "iso_2d_%s_test dataset created" % argvs[1]



# 絶対パス取得(CNNforEM/からスクリプトを走らせる想定)
os.chdir('%s/2014EMneuron/train-input/tif_dataset' % abs_path)
filelist = glob.glob('*.tif') #tif対応

if os.path.exists('%s/2014EMneuron/train-input/tif_dataset/' % abs_path + 'iso_3d_%s_training' % int(argvs[1])):
    shutil.rmtree('iso_3d_%s_training' % argvs[1])

os.mkdir('iso_3d_%s_training' % argvs[1])# saveで自動で作られる可能性がある。その場合不要

f = open('iso_3d_%s_train.txt' % argvs[1], 'w')

print "loading training 3d images..."
file_num = 0
for file in filelist:
    im = Image.open('%s' % file)
    pixels = np.array(list(im.getdata())).reshape((1024, 1024)) # 1024*1024の輝度値を二次元配列として取得
    if file_num == 0:
        ar_3d = pixels
    else:
        ar_3d = np.dstack((ar_3d, pixels))
    file_num += 1
    if file_num % 10 == 0:
        print "%s epoch ended" % file_num
print "loaded"

print "creating iso_3d_%s_training dataset..." % argvs[1]
for i in xrange(0, 100):
    for w in xrange(0, 100):
        for h in xrange(0, 10):
            im_3d = Image.fromarray(np.uint8(ar_3d[i].T))
            range = (w * 10, h * 10, split + 1 + w * 10, split + 1 + h * 10)
            square = im_3d.crop(range)
            ar_square = np.array(list(square.getdata())).reshape((33, 33))
            new_square = np.vstack((ar_square[0:16], ar_square[17:33]))
            ans = ar_square[16][16]
            im_square = Image.fromarray(np.uint8(new_square))
            im_square.save('iso_3d_%s_training/iso_3d' % argvs[1] + '_%03d' % i + '%03d' % w + '%03d' % h + '.tif')
            f.write('iso_3d_%03d' % i + '%03d' % w + '%03d' % h + '.tif' + ' %s\n' % ans)
    if (i + 1) % 10 == 0:
        print "%s epoch ended" % (i + 1)
print "iso_3d_%s_training dataset created" % argvs[1]

os.chdir('%s/2014EMneuron/test-input/tif_dataset' % abs_path)
filelist = glob.glob('*.tif') #tif対応

if os.path.exists('%s/2014EMneuron/test-input/tif_dataset/' % abs_path + 'iso_3d_%s_test' % int(argvs[1])):
    shutil.rmtree('iso_3d_%s_test' % argvs[1])

os.mkdir('iso_3d_%s_test' % argvs[1])# saveで自動で作られる可能性がある。その場合不要

f = open('iso_3d_%s_test.txt' % argvs[1], 'w')

print "loading test 3d images..."
file_num = 0
for file in filelist:
    im = Image.open('%s' % file)
    pixels = np.array(list(im.getdata())).reshape((1024, 1024)) # 1024*1024の輝度値を二次元配列として取得
    if file_num == 0:
        ar_3d = pixels
    else:
        ar_3d = np.dstack((ar_3d, pixels))
    file_num += 1
    if file_num % 10 == 0:
        print "%s epoch ended" % file_num
print "loaded"

print "creating iso_3d_%s_test dataset..." % argvs[1]
for i in xrange(0, 100):
    for w in xrange(0, 100):
        for h in xrange(0, 10):
            im_3d = Image.fromarray(np.uint8(ar_3d[i].T))
            range = (w * 10, h * 10, split + 1 + w * 10, split + 1 + h * 10)
            square = im_3d.crop(range)
            ar_square = np.array(list(square.getdata())).reshape((33, 33))
            new_square = np.vstack((ar_square[0:16], ar_square[17:33]))
            ans = ar_square[16][16]
            im_square = Image.fromarray(np.uint8(new_square))
            im_square.save('iso_3d_%s_test/iso_3d' % argvs[1] + '_%03d' % i + '%03d' % w + '%03d' % h + '.tif')
            f.write('iso_3d_%03d' % i + '%03d' % w + '%03d' % h + '.tif' + ' %s\n' % ans)
    if (i + 1) % 10 == 0:
        print "%s epoch ended" % (i + 1)
print "iso_3d_%s_test dataset created" % argvs[1]
