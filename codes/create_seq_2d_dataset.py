# coding:utf-8
import Image, os, glob, sys
import numpy as np
import shutil

# 絶対パス取得(CNNforEM/からスクリプトを走らせる想定)
abs_path = os.getcwd()
# os.chdir('%s/2014EMneuron/train-input/tif_dataset' % abs_path)
# filelist = glob.glob('*.tif') #tif対応

#コマンドラインからsplitｔを受け取る
argvs = sys.argv #コマンドライン引数のリスト取得
argc = len(argvs) #引数の個数取得
if (argc != 3):
    print 'Usage: コマンドライン引数が' + '%s' % (argc - 1) + 'コで多いもしくは少ないです・・・'
    print '第一引数でパッチサイズを、第二引数で補間幅を指定してください'
    quit()

os.chdir('%s/data/raw_dataset/' % abs_path)
# if os.path.exists('best_seq_2d_%s' % argvs[1] + '_%s_train' % argvs[2]):
#     shutil.rmtree('best_seq_2d_%s' % argvs[1] + '_%s_train' % argvs[2])

os.mkdir('best_seq_2d_%s' % argvs[1] + '_%s_train' % argvs[2])# saveで自動で作られる可能性がある。その場合不要

f = open('best_seq_2d_%s' % argvs[1] + '_%s_train/' % argvs[2] + 'best_seq_2d_%s' % argvs[1] + '_%s_train.txt' % argvs[2], 'w')
split = int(argvs[1])
ans_id = split / 2
blind = (int(argvs[2]) - 1) / 2
file_num = 1

files = np.zeros((100, 512, 512))
for i in xrange(0, 100):
    # 各画像を配列に変換してfiles[i]に保存し、後でImage.fromarrayする
    files[i] = np.array(list(Image.open('%s/2014EMneuron/test-input/median_extract_test_dataset/' % abs_path + 'median_extractImage_%03d.tif' % (int(i) + 1)).getdata())).reshape((512, 512))

print files[0]

print "creating best_seq_2d_%s" % argvs[1] + "_%s_train dataset..." % argvs[2]
for i in xrange(16, 100):
    im = Image.fromarray(np.uint8(files[i]))
    for h in xrange(0, 300):
        for w in xrange(0, 300):
            range = (h * 3, w * 3, split + 1 + h * 3, split + 1 + w * 3)
            square = im.crop(range)
            ar_square = np.array(list(square.getdata())).reshape((split + 1, split + 1))
            new_square = np.vstack((ar_square[0:ans_id - blind], ar_square[ans_id + 1 + blind:split + 1]))
            ans = ar_square[ans_id][ans_id]
            im_square = Image.fromarray(np.uint8(new_square))
            im_square.save('best_seq_2d_%s' % argvs[1] + '_%s_train/best_seq_2d' % argvs[2] + '_%03d' % file_num + '%03d' % w + '%03d' % h + '.tif')
            f.write('best_seq_2d_%03d' % file_num + '%03d' % w + '%03d' % h + '.tif' + ' %s\n' % ans)
    if file_num % 10 == 0:
        print "%s epoch ended" % file_num
    file_num += 1
print "best_seq_2d_%s" % argvs[1] + "_%s_train dataset created" % argvs[2]


# os.chdir('%s/2014EMneuron/test-input/tif_dataset' % abs_path)
# filelist = glob.glob('*.tif') #tif対応

os.chdir('%s/data/raw_dataset/' % abs_path)
# if os.path.exists('best_seq_2d_%s' % argvs[1] + '_%s_test' % argvs[2]):
#     shutil.rmtree('best_seq_2d_%s' % argvs[1] + '_%s_test' % argvs[2])

os.mkdir('best_seq_2d_%s' % argvs[1] + '_%s_test' % argvs[2])# saveで自動で作られる可能性がある。その場合不要

f = open('best_seq_2d_%s' % argvs[1] + '_%s_test/' % argvs[2] + 'best_seq_2d_%s' % argvs[1] + '_%s_test.txt' % argvs[2], 'w')
file_num = 1

print "creating best_seq_2d_%s" % argvs[1] + "_%s_test dataset..." % argvs[2]
for i in xrange(0, 15):
    im = Image.fromarray(np.uint8(files[i]))
    for h in xrange(0, 300):
        for w in xrange(0, 300):
            range = (h * 3, w * 3, split + 1 + h * 3, split + 1 + w * 3)
            square = im.crop(range)
            ar_square = np.array(list(square.getdata())).reshape((split + 1, split + 1))
            new_square = np.vstack((ar_square[0:ans_id - blind], ar_square[ans_id + 1 + blind:split + 1]))
            ans = ar_square[ans_id][ans_id]
            im_square = Image.fromarray(np.uint8(new_square))
            im_square.save('best_seq_2d_%s' % argvs[1] + '_%s_test/best_seq_2d' % argvs[2] + '_%03d' % file_num + '%03d' % w + '%03d' % h + '.tif')
            f.write('best_seq_2d_%03d' % file_num + '%03d' % w + '%03d' % h + '.tif' + ' %s\n' % ans)
    if file_num % 10 == 0:
        print "%s epoch ended" % file_num
    file_num += 1
print "best_seq_2d_%s" % argvs[1] + "_%s_test dataset created" % argvs[2]
