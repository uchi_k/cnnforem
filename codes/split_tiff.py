# coding:utf-8
import sys, os, Image

filename = sys.argv[1]
im = Image.open(filename)

try:
    count = 0
    while 1:
        im.seek(count)# ファイルの第countバイトへ移動
        im.save(os.path.splitext(filename)[0] + "_%04d" % count + ".jpg")
        # ファイル名と拡張子を分ける
        count += 1
except EOFError:# データを読み込まずにEOFに達したらエラー
    pass
