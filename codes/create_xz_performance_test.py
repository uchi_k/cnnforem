# coding:utf-8
import Image, os, glob, sys
import numpy as np
import shutil

#コマンドラインからsplitｔを受け取る
argvs = sys.argv #コマンドライン引数のリスト取得
argc = len(argvs) #引数の個数取得
if (argc != 2):
    print 'Usage: コマンドライン引数が' + '%s' % (argc - 1) + 'コで多いもしくは少ないです・・・'
    quit()

abs_path = os.getcwd()
os.chdir('%s/data/raw_dataset/' % abs_path) # ディレクトリ変更
if os.path.exists('xz_preprocessed_performance_test_%s_dataset' % argvs[1]):
    shutil.rmtree('xz_preprocessed_performance_test_%s_dataset' % argvs[1])
os.mkdir('xz_preprocessed_performance_test_%s_dataset' % argvs[1])

f = open('xz_preprocessed_performance_test_%s_dataset/' % argvs[1] + 'xz_preprocessed_performance_test_%s.txt' % argvs[1], 'w')

split = int(argvs[1]) # 分割サイズを指定
center = split / 2

print "creating xz_preprocessed_performance_test_32 dataset..."
files = np.zeros((100, 512, 512))
for i in xrange(0, 100):
    files[i] = Image.open('%s/' % abs_path + '2014EMneuron/test-input/median_extract_test_dataset/median_extractImage_%03d.tif' % (int(i) + 1))
files

sp_files = np.zeros((100, split + 1, split + 1))
for h in xrange(478):
    range = (0, h, split + 1, h + split + 1)
    for i in xrange(0, 96):
        im_tmp = Image.fromarray(np.uint8(255 * files[i]))
        sp_files[i] = np.array(list(im_tmp.crop(range).getdata())).reshape((split + 1, split + 1))
        im_a = np.hstack((sp_files[i], sp_files[i + 1]))
        im_b = np.hstack((sp_files[i + 3], sp_files[i + 4]))
        im_arr = np.vstack((im_a, im_b))
        im = Image.fromarray(np.uint8(im_arr))
        ans = int(sp_files[i + 2][16][16])
        im.save('xz_preprocessed_performance_test_%s_dataset/' % argvs[1] + 'im_%03d' % i + '%03d.tif' % h)
        f.write('im_%03d' % i + '%03d.tif' % h + ' %s\n' % ans)
    print "%s epoch ended" % h
print "xz_preprocessed_performance_test_%s_dataset created" % argvs[1]
