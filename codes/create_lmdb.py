#coding:utf-8
import lmdb
import re, fileinput, math
import numpy as np

import sys
#caffeのrootにいる前提
sys.path.insert(0, caffe_root + 'python')
import caffe

data = 'train.txt'
lmdb_data_name = 'train_data_lmdb'
lmdb_lebel_name = 'train_score_lmdb'

Input = []
Labels = []

for line in fileinput.input(data):
    entries = re.split(' ', line.strip())
    Inputs.append(entries[0])
    Labels.append(entries[1])
