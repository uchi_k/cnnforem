# -*- coding: utf-8 -*-
import leveldb, os
import numpy as np
import plyvel

abs_path = os.getcwd()
os.chdir('%s/data/raw_dataset/3d_raw_test_32_dataset' % abs_path)
f = open('3d_raw_test_32.txt')

db = leveldb.LevelDB('%s/data/EM_dataset/origin_3d_raw_test_32_leveldb' % abs_path)

batch = leveldb.WriteBatch()
num = 0
for l in f.readlines():
    data = l[:-1].split(' ')
    batch.Put(data[0], data[1])
    if num % 100 == 0:
	print "%s epoch ended" % num
    num += 1
db.Write(batch, sync = True)
