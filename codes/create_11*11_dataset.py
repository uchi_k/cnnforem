# coding:utf-8
import Image, os, glob

# 絶対パス取得(CNNforEM/からスクリプトを走らせる想定)
abs_path = os.getcwd()
os.chdir('%s/2014EMneuron/train-input/' % abs_path)
filelist = glob.glob('*.jpg') #とりあえずjpg対応

if os.path.exists('%s/2014EMneuron/train-input/11datasets' % abs_path) != True:
    os.mkdir('11datasets')# saveで自動で作られる可能性がある。その場合不要

# training dataset
f = open('11train.txt', 'w')
file_num = 0
for file in xrange(len(filelist) - 99):
    im = Image.open('%s' % filelist[file])
    split = 128 # 16分割 → (64 + 1) × (64 + 1)
    height = im.size[1] / split + 1
    width = im.size[0] / split + 1
    h, w =0, 0
    lists = xrange(split)

    for h in lists:
        for w in lists:
            # パッチサイズを奇数にする処理
            # if h == 0 and w == 0:
            #     range = (w * (width - 1), h * (height - 1), (w + 1) * width, (h + 1) * height)
            # elif h == 0 and w == 1:
            #     range = (w * (width - 1) - 1, h * (height - 1), (w + 1) * (width - 1), height)
            # elif h == 1 and w == 0:
            #     range = (0, h * (height - 1) - 1, width, (h + 1) * (height - 1))
            # elif h == 1 and w == 1:
            #     range = (width - 2, height - 2, (w + 1) * (width - 1), (h + 1) * (height - 1))
            #
            # elif w == 0 and h > 1:
            #     range = (0, h * (height - 1), (w + 1) * (width - 1), (h + 1) * (height - 1))
            # elif h == 0 and w > 1:
            #     range = (w * (width - 1), h * (height - 1) - 1, (w + 1) * (width - 1), (h + 1) * (height - 1))
            #
            # else :
            if w == 0:
                range = (0, h * (height - 1) - 1, width, (h + 1) * (height - 1))
            if h == 0:
                range = (w * (width - 1) - 1, 0, (w + 1) * (width - 1), height)
            else :
                range = (w * (width - 1) - 1, h * (height - 1) - 1, (w + 1) * (width - 1), (h + 1) * (height - 1))
            sp_im = im.crop(range)
            # 教師データ準備
            center = ((width + 1) / 2 - 1, (height + 1) / 2 - 1) # 奇数でないと中心が探せない
            ans = sp_im.getpixel((center))
            sp_im.putpixel((center), 0)
            sp_im.save('11datasets/splitImage' + '_%03d' % file_num + '%06d' % h + '%06d' % w + '.jpg')
            f.write('splitImage' + '_%03d' % file_num + '%06d' % h + '%06d' % w + '.jpg' + ' %s\n' % ans)
            if sp_im.size != (width, height):
                print "size error"# 適当・・・try exceptでEOFとか例外処理書いた方が良い
    file_num += 1
    print "1 epoch ended"
print "training dataset created"

        # print range
        # print sp_im.size

# test dataset
os.chdir('%s/2014EMneuron/test-input/' % abs_path)
filelist2 = glob.glob('*.jpg') #とりあえずjpg対応

if os.path.exists('%s/2014EMneuron/test-input/11datasets' % abs_path) != True:
    os.mkdir('11datasets')# saveで自動で作られる可能性がある。その場合不要

f = open('11test.txt', 'w')
file_num = 0
for file in xrange(len(filelist2) - 99):
    im = Image.open('%s' % filelist2[file])
    split = 128 # 128分割 → (8 + 1) × (8 + 1)
    height = im.size[1] / split + 1
    width = im.size[0] / split + 1
    h, w =0, 0
    lists = xrange(split)

    for h in lists:
        for w in lists:
            if w == 0:
                range = (0, h * (height - 1) - 1, width, (h + 1) * (height - 1))
            if h == 0:
                range = (w * (width - 1) - 1, 0, (w + 1) * (width - 1), height)
            else :
                range = (w * (width - 1) - 1, h * (height - 1) - 1, (w + 1) * (width - 1), (h + 1) * (height - 1))
            sp_im = im.crop(range)
            # 教師データ準備
            center = ((width + 1) / 2, (height + 1) / 2) # 奇数の場合のみ考慮
            ans = sp_im.getpixel((center))
            sp_im.putpixel((center), 0)
            sp_im.save('11datasets/splitImage' + '_%03d' % file_num + '%06d' % h + '%06d' % w + '.jpg')
            f.write('splitImage' + '_%03d' % file_num + '%06d' % h + '%06d' % w + '.jpg' + ' %s\n' % ans)
            if sp_im.size != (width, height):
                print "size error"# 適当・・・try exceptでEOFとか例外処理書いた方が良い
    file_num += 1
    print "1 epoch end"
print "test dataset created"
        # print range
        # print sp_im.size
