#coding:utf-8
# coding:utf-8
import Image, os, glob, sys
import numpy as np
import shutil
from sklearn import datasets
import sklearn
from sklearn.decomposition import PCA

# 絶対パス取得(CNNforEM/からスクリプトを走らせる想定)
abs_path = os.getcwd()
os.chdir('%s/2014EMneuron/train-input/median_extract_train_dataset' % abs_path)
filelist = glob.glob('*.tif') #tif対応

#コマンドラインからsplitｔを受け取る
argvs = sys.argv #コマンドライン引数のリスト取得
argc = len(argvs) #引数の個数取得
if (argc != 2):
    print 'Usage: コマンドライン引数が' + '%s' % (argc - 1) + 'コで多いもしくは少ないです・・・'
    quit()

split = int(argvs[1]) # 分割サイズを指定
center = split / 2

os.chdir('%s/data/raw_dataset/' % abs_path) # ディレクトリ変更
if os.path.exists('pca_train_%s_dataset' % argvs[1]):
    shutil.rmtree('pca_train_%s_dataset' % argvs[1])
os.mkdir('pca_train_%s_dataset' % argvs[1])

f = open('pca_train_%s_dataset/' % argvs[1] + 'pca_train_%s.txt' % argvs[1], 'w')

files = np.zeros((100, 512, 512))
for i in xrange(0, 100):
    # 各画像を配列に変換してfiles[i]に保存し、後でImage.fromarrayする
    files[i] = np.array(list(Image.open('%s/2014EMneuron/train-input/median_extract_train_dataset/' % abs_path + 'median_extractImage_%03d.tif' % (int(i) + 1)).getdata())).reshape((512, 512))

print files[0]

### 主成分分析
i = 0
X = [[]]
# 10000枚からPCA行列を作る
for i in xrange(0, 100):
    sp_im = Image.fromarray(np.uint8(files[i]))
    for h in xrange(0, 10):
        for w in xrange(0, 10):
            range = (w  * 45, h * 45, w * 45 + 5, h * 45 + 5)
            crop_sp_im = np.array(list(sp_im.crop(range).getdata())).reshape(1, 25)
            if i == 0 and h == 0 and w == 0:
                X = crop_sp_im
            else:
                X = np.vstack((X, crop_sp_im))
# 平均を算出
X_bar = np.array([row - np.mean(row) for row in X.transpose()]).transpose()
# 共分散行列を求める
m = np.dot(X_bar.T, X_bar) / X.shape[0]
# 固有値問題を解く(wが固有値, vが固有ベクトル)
(w, v) = np.linalg.eig(m)
v = v.T

tmp = {}
for i, value in enumerate(w):
    tmp[value] = i
v_sorted = []
# 固有ベクトルのソート
for key in sorted(tmp.keys(), reverse=True):
    v_sorted.append(v[tmp[key]])
v_sorted = np.array(v_sorted)
w_sorted = np.array(sorted(w, reverse=True))
# 次元削減
dim = 5 #第5主成分まで
components = v_sorted[:dim,]
# pca行列導出(ユニタリ行列なので逆行列は転置されるだけ)
pinv_comp = np.linalg.pinv(components)

# train
print "creating pca_train_%s_dataset..." % argvs[1]
i = 0
for i in xrange(0, 95):
    # 4 + 1枚読み込み(インデックスi+2は教師信号)
    sp_0_im = Image.fromarray(np.uint8(files[i]))
    sp_1_im = Image.fromarray(np.uint8(files[i + 1]))
    sp_2_im = Image.fromarray(np.uint8(files[i + 2]))
    sp_3_im = Image.fromarray(np.uint8(files[i + 3]))
    sp_4_im = Image.fromarray(np.uint8(files[i + 4]))
    for h in xrange(0, 95):
        for w in xrange(0, 95):
            range = (w * 5, h * 5, w * 5 + split + 1, h * 5 + split + 1)
            sp_0_arr = np.array(list(sp_0_im.crop(range).getdata())).reshape((split + 1, split + 1))
            sp_1_arr = np.array(list(sp_1_im.crop(range).getdata())).reshape((split + 1, split + 1))
            sp_3_arr = np.array(list(sp_3_im.crop(range).getdata())).reshape((split + 1, split + 1))
            sp_4_arr = np.array(list(sp_4_im.crop(range).getdata())).reshape((split + 1, split + 1))
            range = (w * 5 + 14, h * 5 + 14, w * 5 + split - 13, h * 5 + split - 13)
            im_a = np.hstack((sp_0_arr, sp_1_arr))
            im_b = np.hstack((sp_3_arr, sp_4_arr))
            im_arr = np.vstack((im_a, im_b))
            im = Image.fromarray(np.uint8(im_arr))
            X = np.array(list(sp_2_im.crop(range).getdata())).reshape((1, 25))[0]
            # 平均を算出
            # X_bar = np.array([row - np.mean(row) for row in X.transpose()]).transpose()
            # 次元削減
            dim = 5
            X_pca = np.dot(X, components.T)
            im.save('pca_train_%s_dataset/' % argvs[1] + 'im_%03d' % i + '%03d' % h + '%03d' % w + '.tif')
            f.write('im_%03d' % i + '%03d' % h + '%03d' % w + '.tif' + " %d " % int(X_pca[0]) +  "%d " % int(X_pca[1]) + \
                    "%d " % int(X_pca[2]) + "%d " % int(X_pca[3]) + "%d\n" % int(X_pca[4]))
    if (i + 1) % 10 == 0:
        print "%s epoch ended" % (i + 1)
print "pca_train_%s_dataset created" % argvs[1]


# test
os.chdir('%s/2014EMneuron/test-input/median_extract_test_dataset' % abs_path)
filelist = glob.glob('*.tif') #tif対応

os.chdir('%s/data/raw_dataset/' % abs_path) # ディレクトリ変更
if os.path.exists('pca_test_%s_dataset' % argvs[1]):
    shutil.rmtree('pca_test_%s_dataset' % argvs[1])
os.mkdir('pca_test_%s_dataset' % argvs[1])

f = open('pca_test_%s_dataset/' % argvs[1] + 'pca_test_%s.txt' % argvs[1], 'w')

files = np.zeros((100, 512, 512))
for i in xrange(0, 100):
    # 各画像を配列に変換してfiles[i]に保存し、後でImage.fromarrayする
    files[i] = np.array(list(Image.open('%s/2014EMneuron/test-input/median_extract_test_dataset/' % abs_path + 'median_extractImage_%03d.tif' % (int(i) + 1)).getdata())).reshape((512, 512))

print files[0]

print "creating pca_test_%s_dataset..." % argvs[1]
i = 0
for i in xrange(0, 95):
    # 4 + 1枚読み込み(インデックスi+2は教師信号)
    sp_0_im = Image.fromarray(np.uint8(files[i]))
    sp_1_im = Image.fromarray(np.uint8(files[i + 1]))
    sp_2_im = Image.fromarray(np.uint8(files[i + 2]))
    sp_3_im = Image.fromarray(np.uint8(files[i + 3]))
    sp_4_im = Image.fromarray(np.uint8(files[i + 4]))
    for h in xrange(0, 95):
        for w in xrange(0, 95):
            range = (w * 5, h * 5, w * 5 + split + 1, h * 5 + split + 1)
            sp_0_arr = np.array(list(sp_0_im.crop(range).getdata())).reshape((split + 1, split + 1))
            sp_1_arr = np.array(list(sp_1_im.crop(range).getdata())).reshape((split + 1, split + 1))
            sp_3_arr = np.array(list(sp_3_im.crop(range).getdata())).reshape((split + 1, split + 1))
            sp_4_arr = np.array(list(sp_4_im.crop(range).getdata())).reshape((split + 1, split + 1))
            range = (w * 5 + 14, h * 5 + 14, w * 5 + split - 13, h * 5 + split - 13)
            im_a = np.hstack((sp_0_arr, sp_1_arr))
            im_b = np.hstack((sp_3_arr, sp_4_arr))
            im_arr = np.vstack((im_a, im_b))
            im = Image.fromarray(np.uint8(im_arr))
            X = np.array(list(sp_2_im.crop(range).getdata())).reshape((1, 25))[0]
            # 平均を算出
            # X_bar = np.array([row - np.mean(row) for row in X.transpose()]).transpose()
            # 次元削減
            dim = 5
            X_pca = np.dot(X, components.T)
            im.save('pca_train_%s_dataset/' % argvs[1] + 'im_%03d' % i + '%03d' % h + '%03d' % w + '.tif')
            f.write('im_%03d' % i + '%03d' % h + '%03d' % w + '.tif' + " %d " % int(X_pca[0]) +  "%d " % int(X_pca[1]) + \
                    "%d " % int(X_pca[2]) + "%d " % int(X_pca[3]) + "%d\n" % int(X_pca[4]))
    if (i + 1) % 10 == 0:
        print "%s epoch ended" % (i + 1)
print "pca_test_%s_dataset created" % argvs[1]
