# coding:utf-8
import Image, os, glob, sys
import numpy as np
import matplotlib.pyplot as plt

# 絶対パス取得(CNNforEM/からスクリプトを走らせる想定)
abs_path = os.getcwd()
os.chdir('%s/2014EMneuron/train-input/tif_dataset' % abs_path)
filelist = glob.glob('*.tif') #tif対応

# for file in filelist:
file_num = 0
for file in filelist:
    im = Image.open('%s' % file)
    pixels = np.array(list(im.getdata())).reshape((1024, 1024)) # 1024*1024の輝度値を二次元配列として取得
    if file_num == 0:
        im_3d = pixels
    else:
        im_3d = np.dstack((im_3d, pixels))
    file_num += 1
    print "%s epoch ended" % file_num

print im_3d
print im_3d[0]
plt.imshow(im_3d[0])
