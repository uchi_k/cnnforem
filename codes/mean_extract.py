# coding:utf-8
import Image, os, glob, sys
import numpy as np

# 絶対パス取得(CNNforEM/からスクリプトを走らせる想定)
abs_path = os.getcwd()
os.chdir('%s/2014EMneuron/train-input/downsampled_tif_dataset' % abs_path)
filelist = glob.glob('*.tif') #tif対応

if os.path.exists('%s/2014EMneuron/train-input/' % abs_path + 'mean_extract_train_dataset') != True:
    os.mkdir('../mean_extract_train_dataset')# saveで自動で作られる可能性がある。その場合不要

# train dataset
N = 0
_sum = 0
for file in filelist:
    im = Image.open('%s' % file)
    im_array = np.array(list(im.getdata()))
    mean = np.mean(im_array) #平均
    _sum += mean
    N += 1
gt_mean = 255 * _sum / N
print gt_mean

file_num = 1
print "creating mean_extract_train_dataset..."
for file in filelist:
    im = Image.open('%s' % file)
    im_array = 255 * np.array(list(im.getdata()))
    mean = np.mean(im_array) #平均
    mean_extract_im_array = (im_array - (mean - gt_mean))
    for i in xrange(512 * 512):
        if mean_extract_im_array[i] < 0:
            mean_extract_im_array[i] = 0
    mean_extract_im = Image.fromarray(np.uint8(mean_extract_im_array.reshape((512, 512))))
    mean_extract_im.save('../mean_extract_train_dataset/mean_extractImage_' + '%03d' % file_num + '.tif')
    file_num += 1
print mean_extract_im_array
if file_num % 10 == 0:
    print "%s epoch ended" % file_num
print "mean_extract_train_dataset created"


os.chdir('%s/2014EMneuron/test-input/downsampled_tif_dataset' % abs_path)
filelist = glob.glob('*.tif') #tif対応

if os.path.exists('%s/2014EMneuron/test-input/' % abs_path + 'mean_extract_test_dataset') != True:
    os.mkdir('../mean_extract_test_dataset')# saveで自動で作られる可能性がある。その場合不要

# test dataset
N = 0
_sum = 0
for file in filelist:
    im = Image.open('%s' % file)
    im_array = np.array(list(im.getdata()))
    mean = np.mean(im_array) #平均
    _sum += mean
    N += 1
gt_mean = 255 * _sum / N
print gt_mean

file_num = 1
print "creating mean_extract_test_dataset..."
for file in filelist:
    im = Image.open('%s' % file)
    im_array = 255 * np.array(list(im.getdata()))
    mean = np.mean(im_array) #平均
    mean_extract_im_array = (im_array - (mean - gt_mean))
    for i in xrange(512 * 512):
        if mean_extract_im_array[i] < 0:
            mean_extract_im_array[i] = 0
    mean_extract_im = Image.fromarray(np.uint8(mean_extract_im_array.reshape((512, 512))))
    mean_extract_im.save('../mean_extract_test_dataset/mean_extractImage_' + '%03d' % file_num + '.tif')
    file_num += 1
print im_array
if file_num % 10 == 0:
    print "%s epoch ended" % file_num
print "mean_extract_test_dataset created"
