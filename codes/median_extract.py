# coding:utf-8
import Image, os, glob, sys
import numpy as np

# 絶対パス取得(CNNforEM/からスクリプトを走らせる想定)
abs_path = os.getcwd()
os.chdir('%s/2014EMneuron/train-input/downsampled_tif_dataset' % abs_path)
filelist = glob.glob('*.tif') #tif対応

if os.path.exists('%s/2014EMneuron/train-input/' % abs_path + 'median_extract_train_dataset') != True:
    os.mkdir('../median_extract_train_dataset')# saveで自動で作られる可能性がある。その場合不要

# train dataset
N = 0
_sum = 0
for file in filelist:
    im = Image.open('%s' % file)
    im_array = np.array(list(im.getdata()))
    median = np.median(im_array) #平均
    _sum += median
    N += 1
    print file
gt_median = 255 * _sum / N
print gt_median

file_num = 1
filelist = glob.glob('*.tif') #tif対応
print "creating median_extract_train_dataset..."
for file in filelist:
    im = Image.open('%s' % file)
    im_array = 255 * np.array(list(im.getdata()))
    median = np.median(im_array) #平均
    median_extract_im_array = (im_array - (median - gt_median))
    for i in xrange(512 * 512):
        if median_extract_im_array[i] < 0:
            median_extract_im_array[i] = 0
    median_extract_im = Image.fromarray(np.uint8(median_extract_im_array.reshape((512, 512))))
    median_extract_im.save('../median_extract_train_dataset/median_extractImage_' + '%03d' % file_num + '.tif')
    file_num += 1
    if file_num % 10 == 0:
        print "%s epoch ended" % file_num

print "median_extract_train_dataset created"


os.chdir('%s/2014EMneuron/test-input/downsampled_tif_dataset' % abs_path)
filelist = glob.glob('*.tif') #tif対応

if os.path.exists('%s/2014EMneuron/test-input/' % abs_path + 'median_extract_test_dataset') != True:
    os.mkdir('../median_extract_test_dataset')# saveで自動で作られる可能性がある。その場合不要

# test dataset
N = 0
_sum = 0
for file in filelist:
    im = Image.open('%s' % file)
    im_array = np.array(list(im.getdata()))
    median = np.median(im_array) #平均
    _sum += median
    N += 1
gt_median = 255 * _sum / N
print gt_median

file_num = 1
filelist = glob.glob('*.tif') #tif対応
print "creating median_extract_test_dataset..."
for file in filelist:
    im = Image.open('%s' % file)
    im_array = 255 * np.array(list(im.getdata()))
    median = np.median(im_array) #平均
    median_extract_im_array = (im_array - (median - gt_median))
    for i in xrange(512 * 512):
        if median_extract_im_array[i] < 0:
            median_extract_im_array[i] = 0
    median_extract_im = Image.fromarray(np.uint8(median_extract_im_array.reshape((512, 512))))
    median_extract_im.save('../median_extract_test_dataset/median_extractImage_' + '%03d' % file_num + '.tif')
    file_num += 1
    if file_num % 10 == 0:
        print "%s epoch ended" % file_num
print "median_extract_test_dataset created"
