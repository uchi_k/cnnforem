# coding:utf-8
import Image, os, glob, sys
import numpy as np

# 絶対パス取得(CNNforEM/からスクリプトを走らせる想定)
abs_path = os.getcwd()
os.chdir('%s/2014EMneuron/train-input/downsampled_tif_dataset' % abs_path)
filelist = glob.glob('*.tif') #tif対応

if os.path.exists('%s/2014EMneuron/train-input/' % abs_path + 'gcn_train_dataset') != True:
    os.mkdir('../gcn_train_dataset')# saveで自動で作られる可能性がある。その場合不要

# train dataset
file_num = 1
print "creating gcn_train_dataset..."
for file in filelist:
    im = Image.open('%s' % file)
    im_array = np.array(list(im.getdata()))
    mean = np.mean(im_array) #平均
    std = np.std(im_array) #標準偏差
    gcn_im_array = (im_array - mean) / std
    gcn_im = Image.fromarray(np.uint8(gcn_im_array.reshape((512, 512))))
    gcn_im.save('../gcn_train_dataset/gcnImage_' + '%d' % file_num + '.tif')
    file_num += 1
print im_array
if file_num % 10 == 0:
    print "%s epoch ended" % file_num
print "gcn_train_dataset created"
