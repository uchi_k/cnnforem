import os, sys
import h5py
import shutil
import sklearn
import tempfile
import numpy as np
import pandas as pd

txt_index = 0
d_tr = np.zeros((2200, 37040), dtype = np.float)
# lab_tr = np.zeros((2200, 1), dtype = np.float32)
lab_tr = []
d_te = np.zeros((200, 37040), dtype = np.float)
# lab_te = np.zeros((200, 1), dtype = np.float32)
lab_te = []
for l in open('data-train.dat').readlines():
    data = l[:-1].split(' ')
    if txt_index < 2200:
        # print txt_index
        # print data
        for size in range(len(data)):
            if size == 0:
               lab_tr.extend([float(data[0])])
            elif txt_index == 341:
               continue
            elif txt_index == 565:
               continue
            elif  txt_index == 1900:
               continue
            else:
               word = data[size].split(':')
               d_tr[txt_index, float(word[0])] = float(word[1])
   else:
       for size in range(len(data)):
            if size == 0:
               lab_te.extend([float(data[0])])
            else:
               word = data[size].split(':')
               d_te[txt_index - 2200, float(word[0])] = float(word[1]    )
       txt_index += 1
print lab_tr
print len(d_tr)
print len(lab_tr)
# Write out the data to HDF5 files in a temp directory.
# This file is assumed to be caffe_root/examples/hdf5_classification.i    pynb
dirname = os.path.abspath('./data')
if not os.path.exists(dirname):
     os.makedirs(dirname)

train_filename = os.path.join(dirname, 'train.h5')
test_filename = os.path.join(dirname, 'test.h5')

# HDF5DataLayer source should be a file containing a list of HDF5 file    names.
# To show this off, we'll list the same data file twice.
with h5py.File(train_filename, 'w') as f:
    f['data'] = d_tr
    f['label'] = lab_tr
with open(os.path.join(dirname, 'train.txt'), 'w') as f:
    f.write(train_filename + '\n')
    f.write(train_filename + '\n')

# HDF5 is pretty efficient, but can be further compressed.
comp_kwargs = {'compression': 'gzip', 'compression_opts': 1}
with h5py.File(test_filename, 'w') as f:
    f.create_dataset('data', data = d_te, **comp_kwargs)
    f.create_dataset('label', data = lab_te, **comp_kwargs)
with open(os.path.join(dirname, 'test.txt'), 'w') as f:
    f.write(test_filename + '\n')
