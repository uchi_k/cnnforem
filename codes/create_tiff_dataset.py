# coding:utf-8
import Image, os, glob, sys

# 絶対パス取得(CNNforEM/からスクリプトを走らせる想定)
abs_path = os.getcwd()
os.chdir('%s/2014EMneuron/train-input/tif_dataset' % abs_path)
filelist = glob.glob('*.tif') #tif対応

#コマンドラインからsplitｔを受け取る
argvs = sys.argv #コマンドライン引数のリスト取得
argc = len(argvs) #引数の個数取得
if (argc != 2):
    print 'Usage: コマンドライン引数が' + '%s' % (argc - 1) + 'コで多いもしくは少ないです・・・'
    quit()

if os.path.exists('%s/2014EMneuron/train-input/' % abs_path + 'tif_dataset/%s_training' % int(argvs[1])) != True:
    os.mkdir('%s_training' % int(argvs[1]))# saveで自動で作られる可能性がある。その場合不要

# training dataset
f = open('%s_train.txt' % int(argvs[1]), 'w')
file_num = 0
for file in filelist:
    im = Image.open('%s' % file)
    split = int(argvs[1]) # n分割 → (n + 1) × (n + 1)
    height = im.size[1] / split + 1
    width = im.size[0] / split + 1
    h, w =0, 0
    lists = xrange(split)

    for h in lists:
        for w in lists:
            # パッチサイズを奇数にする処理
            # if h == 0 and w == 0:
            #     range = (w * (width - 1), h * (height - 1), (w + 1) * width, (h + 1) * height)
            # elif h == 0 and w == 1:
            #     range = (w * (width - 1) - 1, h * (height - 1), (w + 1) * (width - 1), height)
            # elif h == 1 and w == 0:
            #     range = (0, h * (height - 1) - 1, width, (h + 1) * (height - 1))
            # elif h == 1 and w == 1:
            #     range = (width - 2, height - 2, (w + 1) * (width - 1), (h + 1) * (height - 1))
            #
            # elif w == 0 and h > 1:
            #     range = (0, h * (height - 1), (w + 1) * (width - 1), (h + 1) * (height - 1))
            # elif h == 0 and w > 1:
            #     range = (w * (width - 1), h * (height - 1) - 1, (w + 1) * (width - 1), (h + 1) * (height - 1))
            #
            # else :
            if w == 0:
                range = (0, h * (height - 1) - 1, width, (h + 1) * (height - 1))
            if h == 0:
                range = (w * (width - 1) - 1, 0, (w + 1) * (width - 1), height)
            else :
                range = (w * (width - 1) - 1, h * (height - 1) - 1, (w + 1) * (width - 1), (h + 1) * (height - 1))
            sp_im = im.crop(range)
            # 教師データ準備
            center = ((width + 1) / 2 - 1, (height + 1) / 2 - 1) # 奇数でないと中心が探せない
            ans = sp_im.getpixel((center))
            sp_im.putpixel((center), 0)
            sp_im.save('%s_training/splitImage' % int(argvs[1]) + '_%04d' % file_num + '%04d' % h + '%04d' % w + '.tif')
            f.write('splitImage' + '_%04d' % file_num + '%04d' % h + '%04d' % w + '.tif' + ' %s\n' % ans)
            if sp_im.size != (width, height):
                print "size error"# 適当・・・try exceptでEOFとか例外処理書いた方が良い
    file_num += 1
    print "%s epoch ended" % file_num
print "training dataset created"
        # print range
        # print sp_im.size

# test dataset
os.chdir('%s/2014EMneuron/test-input/tif_dataset' % abs_path)
filelist = glob.glob('*.tif') #とりあえずjpg対応

if os.path.exists('%s/2014EMneuron/test-input/tif_dataset/' % abs_path + '%s_test' % int(argvs[1])) != True:
    os.mkdir('%s_test' % int(argvs[1]))# saveで自動で作られる可能性がある。その場合不要

f = open('%s_test.txt' % int(argvs[1]), 'w')
file_num = 0
for file in filelist:
    im = Image.open('%s' % file)
    split = int(argvs[1]) # n分割 → (n + 1) × (n + 1)
    height = im.size[1] / split + 1
    width = im.size[0] / split + 1
    h, w =0, 0
    lists = xrange(split)

    for h in lists:
        for w in lists:
            if w == 0:
                range = (0, h * (height - 1) - 1, width, (h + 1) * (height - 1))
            if h == 0:
                range = (w * (width - 1) - 1, 0, (w + 1) * (width - 1), height)
            else :
                range = (w * (width - 1) - 1, h * (height - 1) - 1, (w + 1) * (width - 1), (h + 1) * (height - 1))
            sp_im = im.crop(range)
            # 教師データ準備
            center = ((width + 1) / 2 - 1, (height + 1) / 2 - 1) # 奇数の場合のみ考慮
            ans = sp_im.getpixel((center))
            sp_im.putpixel((center), 0)
            sp_im.save('%s_test/splitImage' % int(argvs[1]) + '_%04d' % file_num + '%04d' % h + '%04d' % w + '.tif')
            f.write('splitImage' + '_%04d' % file_num + '%04d' % h + '%04d' % w + '.tif' + ' %s\n' % ans)
            if sp_im.size != (width, height):
                print "size error"# 適当・・・try exceptでEOFとか例外処理書いた方が良い
    file_num += 1
    print "%s epoch ended" % file_num
print "test dataset created"
        # print range
        # print sp_im.size
