# coding:utf-8
import Image, os, glob, sys
import numpy as np
import shutil

# train
# 絶対パス取得(CNNforEM/からスクリプトを走らせる想定)
abs_path = os.getcwd()
os.chdir('%s/2014EMneuron/test-input/median_extract_test_dataset' % abs_path) #testデータを分割してトレーニングを行う
filelist = glob.glob('*.tif') #tif対応

#コマンドラインからsplitｔを受け取る
argvs = sys.argv #コマンドライン引数のリスト取得
argc = len(argvs) #引数の個数取得
if (argc != 2):
    print 'Usage: コマンドライン引数が' + '%s' % (argc - 1) + 'コで多いもしくは少ないです・・・'
    quit()

split = int(argvs[1]) # 分割サイズを指定
center = split / 2

os.chdir('%s/data/raw_dataset/' % abs_path) # ディレクトリ変更
# if os.path.exists('sequence_only_train_%s_dataset' % argvs[1]):
#     shutil.rmtree('sequence_only_train_%s_dataset' % argvs[1])
os.mkdir('sequence_only_train_%s_dataset' % argvs[1])

f = open('sequence_only_train_%s_dataset/' % argvs[1] + 'sequence_only_train_%s.txt' % argvs[1], 'w')

files = np.zeros((100, 512, 512))
for i in xrange(0, 100):
    # 各画像を配列に変換してfiles[i]に保存し、後でImage.fromarrayする
    files[i] = np.array(list(Image.open('%s/2014EMneuron/test-input/median_extract_test_dataset/' % abs_path + 'median_extractImage_%03d.tif' % (int(i) + 1)).getdata())).reshape((512, 512))

print files[0]

print "creating sequence_only_train_%s_dataset..." % argvs[1]
i = 0
for i in xrange(20, 95):
    # 4 + 1枚読み込み(インデックスi+2は教師信号)
    sp_0_im = Image.fromarray(np.uint8(files[i]))
    sp_1_im = Image.fromarray(np.uint8(files[i + 1]))
    sp_2_im = Image.fromarray(np.uint8(files[i + 2]))
    sp_3_im = Image.fromarray(np.uint8(files[i + 3]))
    sp_4_im = Image.fromarray(np.uint8(files[i + 4]))
    for h in xrange(0, 480):
        for w in xrange(0, 480):
            range = (w, h, w + split + 1, h + split + 1)
            sp_0_arr = np.array(list(sp_0_im.crop(range).getdata())).reshape((split + 1, split + 1))
            sp_1_arr = np.array(list(sp_1_im.crop(range).getdata())).reshape((split + 1, split + 1))
            sp_3_arr = np.array(list(sp_3_im.crop(range).getdata())).reshape((split + 1, split + 1))
            sp_4_arr = np.array(list(sp_4_im.crop(range).getdata())).reshape((split + 1, split + 1))
            im_a = np.hstack((sp_0_arr, sp_1_arr))
            im_b = np.hstack((sp_3_arr, sp_4_arr))
            im_arr = np.vstack((im_a, im_b))
            im = Image.fromarray(np.uint8(im_arr))
            ans = np.array(list(sp_2_im.crop(range).getdata())).reshape((split + 1, split + 1))[center][center]
            im.save('sequence_only_train_%s_dataset/' % argvs[1] + 'im_%03d' % i + '%03d' % h + '%03d' % w + '.tif')
            f.write('im_%03d' % i + '%03d' % h + '%03d' % w + '.tif' + ' %s\n' % ans)
    if (i + 1) % 10 == 0:
        print "%s epoch ended" % (i + 1)


# test
os.chdir('%s/data/raw_dataset/' % abs_path) # ディレクトリ変更
# if os.path.exists('sequence_only_test_%s_dataset' % argvs[1]):
#     shutil.rmtree('sequence_only_test_%s_dataset' % argvs[1])
os.mkdir('sequence_only_test_%s_dataset' % argvs[1])

f = open('sequence_only_test_%s_dataset/' % argvs[1] + 'sequence_only_test_%s.txt' % argvs[1], 'w')

print "creating sequence_only_test_%s_dataset..." % argvs[1]
i = 0
for i in xrange(0, 15):
    # 4 + 1枚読み込み(インデックスi+2は教師信号)
    sp_0_im = Image.fromarray(np.uint8(files[i]))
    sp_1_im = Image.fromarray(np.uint8(files[i + 1]))
    sp_2_im = Image.fromarray(np.uint8(files[i + 2]))
    sp_3_im = Image.fromarray(np.uint8(files[i + 3]))
    sp_4_im = Image.fromarray(np.uint8(files[i + 4]))
    for h in xrange(0, 480):
        for w in xrange(0, 480):
            range = (w, h, w + split + 1, h + split + 1)
            sp_0_arr = np.array(list(sp_0_im.crop(range).getdata())).reshape((split + 1, split + 1))
            sp_1_arr = np.array(list(sp_1_im.crop(range).getdata())).reshape((split + 1, split + 1))
            sp_3_arr = np.array(list(sp_3_im.crop(range).getdata())).reshape((split + 1, split + 1))
            sp_4_arr = np.array(list(sp_4_im.crop(range).getdata())).reshape((split + 1, split + 1))
            im_a = np.hstack((sp_0_arr, sp_1_arr))
            im_b = np.hstack((sp_3_arr, sp_4_arr))
            im_arr = np.vstack((im_a, im_b))
            im = Image.fromarray(np.uint8(im_arr))
            ans = np.array(list(sp_2_im.crop(range).getdata())).reshape((split + 1, split + 1))[center][center]
            im.save('sequence_only_test_%s_dataset/' % argvs[1] + 'im_%03d' % i + '%03d' % h + '%03d' % w + '.tif')
            f.write('im_%03d' % i + '%03d' % h + '%03d' % w + '.tif' + ' %s\n' % ans)
    if (i + 1) % 10 == 0:
        print "%s epoch ended" % (i + 1)
print "sequence_only_test_%s_dataset created" % argvs[1]
