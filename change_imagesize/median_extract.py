# coding:utf-8
import os, glob, sys
import numpy as np
from PIL import Image

argvs = sys.argv
argc = len(argvs)
if (argc != 2):
    print 'Usage: コマンドライン引数が%sコで多いもしくは少ないです...画像の大きさを入力してください' % (argc - 1)
    quit()

im_size = int(argvs[1])
downsample_rate = 1024 / im_size

# 絶対パス取得(CNNforEM/からスクリプトを走らせる想定)
abs_path = os.getcwd()
os.chdir('%s/2014EMneuron/train-input/downsampled_%s_tif_dataset' % (abs_path, downsample_rate))
filelist = glob.glob('*.tif') #tif対応

if os.path.exists('%s/2014EMneuron/train-input/sizechange/' % abs_path + 'median_extract_%s_train_dataset' % downsample_rate) != True:
    os.mkdir('../sizechange/median_extract_%s_train_dataset' % downsample_rate)# saveで自動で作られる可能性がある。その場合不要

# train dataset
N = 0
_sum = 0
for file in filelist:
    im = Image.open('%s' % file)
    im_array = np.array(list(im.getdata()))
    median = np.median(im_array) #平均
    _sum += median
    N += 1
    print file
gt_median = 255 * _sum / N
print gt_median

file_num = 1
filelist = glob.glob('*.tif') #tif対応
print "creating median_extract_%s_train_dataset..."
for file in filelist:
    im = Image.open('%s' % file)
    im_array = 255 * np.array(list(im.getdata()))
    median = np.median(im_array) #平均
    median_extract_im_array = (im_array - (median - gt_median))
    for i in xrange(im_size * im_size):
        if median_extract_im_array[i] < 0:
            median_extract_im_array[i] = 0
    median_extract_im = Image.fromarray(np.uint8(median_extract_im_array.reshape((im_size, im_size))))
    median_extract_im.save('../sizechange/median_extract_%s_train_dataset/median_extractImage_' % downsample_rate + '%03d' % file_num + '.tif')
    file_num += 1
    if file_num % 10 == 0:
        print "%s epoch ended" % file_num

print "median_extract_%s_train_dataset created" % downsample_rate


os.chdir('%s/2014EMneuron/test-input/downsampled_%s_tif_dataset' % (abs_path, downsample_rate))
filelist = glob.glob('*.tif') #tif対応

if os.path.exists('%s/2014EMneuron/test-input/sizechange/' % abs_path + 'median_extract_%s_test_dataset' % downsample_rate) != True:
    os.mkdir('../sizechange/median_extract_%s_test_dataset' % downsample_rate)# saveで自動で作られる可能性がある。その場合不要

# test dataset
N = 0
_sum = 0
for file in filelist:
    im = Image.open('%s' % file)
    im_array = np.array(list(im.getdata()))
    median = np.median(im_array) #平均
    _sum += median
    N += 1
gt_median = 255 * _sum / N
print gt_median

file_num = 1
filelist = glob.glob('*.tif') #tif対応
print "creating median_extract_%s_test_dataset..." % downsample_rate
for file in filelist:
    im = Image.open('%s' % file)
    im_array = 255 * np.array(list(im.getdata()))
    median = np.median(im_array) #平均
    median_extract_im_array = (im_array - (median - gt_median))
    for i in xrange(im_size * im_size):
        if median_extract_im_array[i] < 0:
            median_extract_im_array[i] = 0
    median_extract_im = Image.fromarray(np.uint8(median_extract_im_array.reshape((im_size, im_size))))
    median_extract_im.save('../sizechange/median_extract_%s_test_dataset/median_extractImage_' % downsample_rate + '%03d' % file_num + '.tif')
    file_num += 1
    if file_num % 10 == 0:
        print "%s epoch ended" % file_num
print "median_extract_%s_test_dataset created" % downsample_rate
