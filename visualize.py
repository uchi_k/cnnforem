import numpy as np
import matplotlib.pyplot as plt

# Make sure that caffe is on the python path:
import sys
sys.path.insert(0, 'python')

import caffe

plt.rcParams['figure.figsize'] = (10, 10)
plt.rcParams['image.interpolation'] = 'nearest'
plt.rcParams['image.cmap'] = 'gray'

net = caffe.Classifier('EMmodel/CNNforEM_deploy.prototxt', 'EMmodel/_iter_10000.caffemodel')

[(k, v.data.shape) for k, v in net.blobs.items()]


