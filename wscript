import maf
import maflib
import util

VERSION = '0.0.0'
APPNAME = 'pydata_demo'

def options(opt):
    opt.load('maf')

def configure(conf):
    conf.load('maf')

def build(bld):
    CAFFE_ROOT = '/home/uchihashi-k/CNNforEM' # Fill in the path according to your environment
    TRAIN_DATA = '%s/examples/mnist/mnist_train_lmdb' % CAFFE_ROOT
    DEV_DATA = '%s/examples/mnist/mnist_test_lmdb' % CAFFE_ROOT
    # MEAN_FILE = '%s/examples/cifar10/mean.binaryproto' % CAFFE_ROOT

    bld(source = '../models/mnist/data.prototxt.base',
        target = '../maf/mnist/prototxt/part/data.prototxt',
        rule = util.prototxt.generate_datalayer,
        parameters = maflib.util.product({'train_data' : [TRAIN_DATA],
                                          'dev_data' : [DEV_DATA]}))

    bld(source = '../models/mnist/net.prototxt.base',
        target = '../maf/mnist/prototxt/part/net.prototxt',
        rule = util.prototxt.generate_net,
        parameters = maflib.util.sample(10, {'conv_num_output_1' : [32, 64], 'conv_num_output_2' : [32, 64],
                                                'conv_kernel_size_1' : [3, 5], 'conv_kernel_size_2' : [3, 5],
                                                'pool_1' : ['MAX', 'AVE'], 'pool_2' : ['MAX', 'AVE'],}))

    bld(source = '../maf/mnist/prototxt/part/data.prototxt ../maf/mnist/prototxt/part/net.prototxt ../models/mnist/loss.prototxt',
        target = '../maf/mnist/prototxt/part/train.prototxt',
        rule = 'cat ${SRC} > ${TGT}')

    bld(source = '../models/mnist/solver.prototxt.base ../maf/mnist/prototxt/part/train.prototxt',
        target = '../maf/mnist/prototxt/solver.prototxt ../maf/mnist/snapshot/train',
        rule = util.prototxt.generate_solver,
        parameters = maflib.util.product({'base_lr' : [0.001, 0.01, 0.1], 'momentum' : [0.7, 0.9], 'weight_decay' : [0.0001, 0.001],
					                       'test_iter' : [100], 'test_interval' : [1000],
                                          'lr_policy' : ['inv'], 'gamma': [0.0001], 'power': [0.75],
                                          'display' : [100], 'max_iter' : [10000], 'snapshot' : [5000], 'solver_mode' : ['GPU']}))

    bld(source = '../models/mnist/blob.prototxt ../maf/mnist/prototxt/part/net.prototxt ../models/mnist/softmax.prototxt',
        target = '../maf/mnist/prototxt/dev.prototxt',
        rule = 'cat ${SRC} > ${TGT}')

    bld(source = '../maf/mnist/prototxt/solver.prototxt',
        target = '../maf/mnist/log/train.log',
        rule = 'tools/caffe train -solver=${SRC} > ${TGT} 2>&1')

    bld(source = '../maf/mnist/log/train.log',
        target = '../maf/mnist/result/loss.json',
        rule = util.log.extract_loss)

    bld(source = '../maf/mnist/result/loss.json',
        target = '../maf/mnist/result/loss.png',
        rule = util.plot.plot,
        aggregate_by = 'base_lr momentum weight_decay')

    # IMAGE_FILE = 'data/cifar-10-batches-py/data_batch_1'
    # bld(source = 'prototxt/dev.prototxt snapshot/train log/train.log',
    #     target = 'result/feature.png',
    #     rule = util.classify.classify(image_file = IMAGE_FILE))
