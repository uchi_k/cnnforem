# coding:utf-8
from PIL import Image
import os, glob, sys
import numpy as np
import matplotlib.pyplot as plt

from theano.tensor.signal import downsample
import theano.tensor as T
import theano

# 絶対パス取得(CNNforEM/からスクリプトを走らせる想定)
abs_path = os.getcwd()
os.chdir('%s/2014EMneuron/train-input/tif_dataset' % abs_path)
filelist = glob.glob('*.tif') #tif対応

#コマンドラインからpoolsize(maxpool_shape)を受け取る
argvs = sys.argv #コマンドライン引数のリスト取得
argc = len(argvs) #引数の個数取得
if (argc != 2):
    print 'Usage: コマンドライン引数が' + '%s' % (argc - 1) + 'コしか指定されていません・・・'
    quit()

poolsize = int(argvs[1])

if os.path.exists('%s/2014EMneuron/train-input/sizechange/downsampled_%s_tif_dataset' % (abs_path, argvs[1])) != True:
    os.mkdir('../sizechange/downsampled_%s_tif_dataset' % argvs[1]) # データ置き場用意

file_num = 1
for file in filelist:
    im = Image.open(file)
    pixels = np.array(list(im.getdata())).reshape((1, 1, 1024, 1024)) # theanoは4darrayを扱うので一時的に変形

    input = T.dtensor4('input')
    maxpool_shape = (poolsize, poolsize)
    pool_out = downsample.max_pool_2d(input, maxpool_shape, ignore_border = True) # 奇数端は切り捨て
    f = theano.function([input],pool_out)
    pooled_im = Image.fromarray(np.array(f(pixels)).squeeze() / 255)
    pooled_im.save('../sizechange/downsampled_%s_tif_dataset/ds_tif' % argvs[1] + '_%03d' % file_num + '.tif')
    file_num += 1
    if file_num % 10 == 0:
        print "%s epoch ended" % file_num
print "training 100 raw tif data created"

# 絶対パス取得(CNNforEM/からスクリプトを走らせる想定)
os.chdir('%s/2014EMneuron/test-input/tif_dataset' % abs_path)
filelist = glob.glob('*.tif') #tif対応

if os.path.exists('%s/2014EMneuron/test-input/sizechange/downsampled_%s_tif_dataset' % (abs_path, argvs[1])) != True:
    os.mkdir('../sizechange/downsampled_%s_tif_dataset' % argvs[1]) # データ置き場用意

file_num = 1
for file in filelist:
    im = Image.open(file)
    pixels = np.array(list(im.getdata())).reshape((1, 1, 1024, 1024)) # theanoは4darrayを扱うので一時的に変形

    input = T.dtensor4('input')
    maxpool_shape = (poolsize, poolsize)
    pool_out = downsample.max_pool_2d(input, maxpool_shape, ignore_border = True) # 奇数端は切り捨て
    f = theano.function([input],pool_out)
    pooled_im = Image.fromarray(np.array(f(pixels)).squeeze() / 255)
    pooled_im.save('../sizechange/downsampled_%s_tif_dataset/ds_tif' % argvs[1] + '_%03d' % file_num + '.tif')
    file_num += 1
    if file_num % 10 == 0:
        print "%s epoch ended" % file_num
print "test 100 raw tif data created"
# print 'With ignore_border set to True:'
# print 'pixels =\n', pixels
# print 'pooled pixels =\n', pooled_im
# print 'size =\n', f(pixels).size
