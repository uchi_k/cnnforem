import maf
import maflib
import util

VERSION = '0.0.0'

def options(opt):
    opt.load('maf')

def configure(conf):
    conf.load('maf')

def build(bld):
    NAME = 'CNN'
    CAFFE_ROOT = '/home/uchihashi-k/CNNforEM'
    TRAIN_DATA = '%s/data/EM_dataset/large_mean_extract_training_32_lmdb' % CAFFE_ROOT
    DEV_DATA = '%s/data/EM_dataset/large_mean_extract_test_32_lmdb' % CAFFE_ROOT

    #train_test
    bld(source = '../template/data.prototxt.base',
        target = '../part/data.prototxt',
        rule = util.prototxt.generate_datalayer,
        parameters = maflib.util.product({'name' : [NAME],
                                          'train_data' : [TRAIN_DATA],
                                          'dev_data' : [DEV_DATA]}))

    bld(source = '../part/data.prototxt ../template/net.prototxt ../template/loss.prototxt',
        target = '../CNN_32_train_test.prototxt',
        rule = 'cat ${SRC} > ${TGT}')

    #solver
    bld(source = '../template/solver.prototxt.base ../CNN_32_train_test.prototxt',
        target = '../CNN_32_solver.prototxt 64_snapshot',
        rule = util.prototxt.generate_solver,
        parameters = maflib.util.product({'base_lr' : [0.00000001], 'momentum' : [0.9], 'weight_decay' : [0.0005],
                                     'test_iter' : [1000], 'test_interval' : [5000],
                                          'lr_policy' : ['inv'], 'gamma': [0.0001], 'power': [0.75],
                                          'display' : [100], 'max_iter' : [100000], 'snapshot' : [50000], 'solver_mode' : ['GPU']}))

    #deploy
    bld(source = '../template/blob.prototxt ../template/net.prototxt',
        target = '../CNN_32_deploy.prototxt',
        rule = 'cat ${SRC} > ${TGT}')

    #training start
    bld(source = '../CNN_32_solver.prototxt',
        target = 'log_64/train.log',
        rule = '../../../../build/tools/caffe train -solver=${SRC} > ${TGT} 2>&1')

    bld(source = 'log_64/train.log',
        target = 'result_64/loss.json',
        rule = util.log.extract_loss)

    bld(source = 'result_64/loss.json',
        target = 'result_64/loss.png',
        rule = util.plot.plot,
        aggregate_by = 'name')
